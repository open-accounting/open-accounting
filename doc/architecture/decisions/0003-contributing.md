# 3. Contributing

Date: 2019-10-25

## Status

Accepted

## Context

To be an effective team in contributing, we need to structure our contributions to the open-accounting project.

## Decision

We follow these conventions and agreements when contributing to open-accounting.
- All contributions to open-accounting fall under the Apache 2.0 license.
- We document our way of working in `CONTRIBUTING.md`
- We structure our User stories using the Issue Template in `User story.md`

## Consequences

We can focus our contribution efforts on the features of the product.
