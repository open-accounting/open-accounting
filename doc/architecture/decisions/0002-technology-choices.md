# 3. Technology choices

Date: 2018-10-25

## Status

Proposed

## Context

Some technology should be used to build the software we want to build. While we don't want to make decisions about every little thing, we need to decide on the significant ones.

## Decision

The technology stack we start of with is:
- React on the frontend
- Spring Boot 2 on the backend, using the Kotlin language
- GraphQL to communicate between backend and frontend

Also we:
- structure our backend code in such a way that it will be possible to easily split of parts of the code later on. This will mean we structure application packages by feature/component. As rule of thumb: a package should be the size of a bounded context as defined by Domain Driven Design. See [Layers, hexagons, feature and components](http://www.codingthearchitecture.com/2016/04/25/layers_hexagons_features_and_components.html) for more info
- follow git branch and commit naming as proposed [here](https://stackoverflow.com/a/6065944) and [here](https://medium.com/@menuka/writing-meaningful-git-commit-messages-a62756b65c81)
- use [Github Flow](https://guides.github.com/introduction/flow/), meaning we create feature branches based on `master`, do code reviews on those feature branches en merge those to `master` when approved. So we **don't** use GitFlow

## Consequences

It will become obvious on what significant technology to use.
