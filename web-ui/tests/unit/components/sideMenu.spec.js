import { shallowMount, createLocalVue } from "@vue/test-utils";
import Vuetify from "vuetify";
import SideMenu from "@/components/SideMenu.vue";

describe("SideMenu.vue", () => {
  let wrapper;
  beforeEach(() => {
    const localVue = createLocalVue();
    localVue.use(Vuetify);

    wrapper = shallowMount(SideMenu, {
      localVue
    });
  });

  it("should render correctly", () => {
    expect(wrapper.element).toMatchSnapshot();
  });
});
