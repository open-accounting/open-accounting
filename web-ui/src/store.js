import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    journal: []
  },
  mutations: {
    addJournalEntry(state, entry) {
      state.journal.push(entry);
    }
  },
  actions: {}
});
