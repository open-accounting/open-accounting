# web-ui
This is the web UI for the [open-accounting](https://gitlab.com/open-accounting/) application. It is based on Vue.js, being initialized using the [vue-cli](https://cli.vuejs.org/). The setup configuration can be found as `cli-config-1.png` and `cli-config-2.png` in the `docs` folder.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
