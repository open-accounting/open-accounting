# flutter_ui

A flutter ui for open-accounting

## Getting Started

To set up flutter, follow the guides for your platform at https://flutter.dev/docs/get-started/install

Afterwards, make sure there is an emulator running, and run

  $ `flutter run`
