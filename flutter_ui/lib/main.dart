import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import 'views/expenses/expense_screen.dart';
import 'views/expenses/expenses_screen.dart';
import 'views/home_screen.dart';

import 'data/graphqlconf.dart';

GraphQLConfiguration graphQLConfiguration = GraphQLConfiguration();

void main() => runApp(
  GraphQLProvider(
    client: graphQLConfiguration.client,
    child: CacheProvider(child: MyApp()),
  )
);

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Open Accounting',
      theme: ThemeData(
        primarySwatch: Colors.grey,
      ),
      routes: {
        HomeScreen.routeName: (context) => HomeScreen(),
        ExpensesScreen.routeName: (context) => ExpensesScreen(),
        ExpenseScreen.routeName: (context) => ExpenseScreen(),
      },
      initialRoute: HomeScreen.routeName,
    );
  }
}
