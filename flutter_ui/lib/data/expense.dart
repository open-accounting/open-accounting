class Expense {
  Expense(this.id, this.name);

  final String id;
  final String name;

  getId() => this.id;
  getName() => this.name;

  // Deserialize
  static fromJson(expenseJson) => new Expense(expenseJson['id'], expenseJson['name']);

  // Graphql Queries
  static String fetchAll = """
    query get_all_expenses {
      openaccounting_expense {
        name
        id
      }
    }
  """;
}