import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  static const routeName = '/';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Open Accounting "),
        backgroundColor: Colors.grey[400],
      ),
      body: ListView(
      padding: const EdgeInsets.all(8),
      children: 
        <Widget>[
          InkWell(
            child: Container(
              height: 100,
              color: Colors.red[200],
              child: const Center(child: Text('Uren', style: TextStyle(fontSize: 22),)),
            ),
            onTap: () {
              Navigator.pushNamed(context, '/hours');
            },
          ),
          InkWell(
            child: Container(
              height: 100,
              color: Colors.green[300],
              child: const Center(child: Text('Uitgaven', style: TextStyle(fontSize: 22),)),
            ),
            onTap: () {
              Navigator.pushNamed(context, '/expenses');
            },
          ),
          InkWell(
            child: Container(
              height: 100,
              color: Colors.blue[200],
              child: const Center(child: Text('Inkomsten', style: TextStyle(fontSize: 22),)),
            ),
            onTap: () {
              Navigator.pushNamed(context, '/turnover');
            },
          ),
          InkWell(
            child: Container(
              height: 100,
              color: Colors.orange[200],
              child: const Center(child: Text('Rapporten', style: TextStyle(fontSize: 22),)),
            ),
            onTap: () {
              Navigator.pushNamed(context, '/reports');
            },
          ),
        ],
      ),
    );
  }
}