import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import 'expense_screen.dart';

import '../../data/expense.dart';

class ExpensesScreen extends StatefulWidget {
  static const routeName = '/expenses';

  @override
  _ExpensesScreenState createState() => _ExpensesScreenState();
}

class _ExpensesScreenState extends State<ExpensesScreen> {

  VoidCallback refetchQuery;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Open Accounting "),
        backgroundColor: Colors.grey[400],
      ),
      body: Query(
        options: QueryOptions(
          documentNode: gql(Expense.fetchAll),
        ),
        builder: (QueryResult result, { VoidCallback refetch, FetchMore fetchMore }) {
          if (result.hasException) {
            print(result.exception.toString() + "");
            return Text(result.exception.toString());
          }
          if (result.loading) {
            return Text('Loading');
          }

          List expensesResult = result.data['openaccounting_expense'];
          return ListView.builder(
            itemCount: expensesResult.length,
            itemBuilder: (context, index) {
              Expense expense = Expense.fromJson(expensesResult[index]);
              print(expense);
              return InkWell(
                child: Container(
                  height: 100,
                  color: Colors.white,
                  child: Center(child: Text(expense.name, style: TextStyle(fontSize: 22),)),
                ),
                onTap: () {
                  // Navigator.pushNamed(context, '/expense/' + expense.id.toString());
                  Navigator.pushNamed(context, ExpenseScreen.routeName, arguments: expense.id);
                }                                  
              );
            },
          );
        }
      ),
    );
  }
}