import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ExpenseScreen extends StatefulWidget {
  static const routeName = '/expense';

  @override
  _ExpenseScreenState createState() => _ExpenseScreenState();
}

class _ExpenseScreenState extends State<ExpenseScreen> {

  VoidCallback refetchQuery;
  @override
  Widget build(BuildContext context) {
    final String id = ModalRoute.of(context).settings.arguments;
    
    return Scaffold(
      appBar: AppBar(
        title: Text("Expense Details"),
        backgroundColor: Colors.red[500],
      ),
      body: Text("TODO show details about expense with id " + id),
    );
  }
}