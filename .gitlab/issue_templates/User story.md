**As a**
<br>**I want to**
<br>**so that**

**:white\_check\_mark: Acceptance criteria**

1.
1.

**:hammer\_and\_wrench: Proposed technical solution**

*

**:link: Reference links**

* 
* 

Note: see the Definition of Done for general story requirements: [Project Definition of Done](../../CONTRIBUTING.md#definition-of-done#42-definition-of-done)