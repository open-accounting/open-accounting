# Contributing

We are building an open source accounting system using modern web technologies.
This guide describes our way of working

We love issues and merge requests from everyone.

## Problems, suggestions and questions in Issues

You don't need to change any of our code or documentation to be a contributor. Please help development by reporting problems, suggesting changes and asking questions. To do this, you can [create an issue using GitLab](https://docs.gitlab.com/ee/user/project/issues/create_new_issue.html) for this project in the [GitLab Issues for Open Accounting](https://gitlab.com/open-accounting/open-accounting/issues).

### Issue template

We use an issue template for all issues. Currently there is only one template, the User Story template. This template helps you think from the user perspective: 'who wants this new feature and why?'. Always write user stories in terms of business value. We will add issue templates for new types of issues such as Bugs when we need them.

## Documentation and code in Merge Requests

If you want to add to the documentation or code of one of our projects you should push a branch and make a Merge Request.

### 1. Make your changes

#### 1.1. Use GitHub flow
This project uses the **GitHub flow** workflow. When you've forked this repository, please make sure to create a feature branch following the GitHub flow model. Read this [short blogpost](https://guides.github.com/introduction/flow/) when you're not yet familiar with GitHub flow.

#### 1.2. Add docs and tests
If you are adding code, make sure you've added and updated the relevant documentation and tests before you submit your Merge Request. Make sure to write unit tests that show the behaviour of the newly added or changed code.

### 2. Commit messages

#### 2.1. Explain your contributions
Add your changes in commits [with a message that explains them](https://robots.thoughtbot.com/5-useful-tips-for-a-better-commit-message). Document choices or decisions you make in the commit message, this will enable everyone to be informed of your choices in the future.

#### 2.2. Release schedule
We dont' have a release schedule yet, as this software is in an experimental phase.

#### 2.3. Conventions for commit messages
We follow the [Angular Commit Message conventions](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines). This convention requires you to pass a subject and scope in the commit message. The scope is based on the components in the repository. If you are not sure which scope to use please leave the scope empty.

The available scopes are:

- ui
- backend

#### 2.4. Do NOT close issues with commit messages
Make sure _not_ to use the commit message to [automatically close issues](https://docs.gitlab.com/ee/user/project/issues/automatic_issue_closing.html), since we do _not_ want issues to be closed immediately after merging to the master branch.

### 3. Merge Request

#### 3.1. Always refer to an issue
Before starting a Merge Request, make sure there are one or more User Stories describing what you want to achieve with the MR. [Create user stories by submitting a new issue](https://gitlab.com/open-accounting/open-accounting/issues) if there are none.

#### 3.2. Describe the MR

When submitting the Merge Request, please accompany it with a short description of the problem you are trying to address and the issue numbers that this Merge Request fixes/addresses.

#### 3.3. Combine frontend and backend work in one MR
When working on a feature which requires specific knowledge of multiple disciplines (eg. both frontend & backend), make sure to complete your MR before asking for a review.
By doing so, the reviewer can consider the complete solution and give more insightful feedback.

### 4. Improve

#### 4.1. Reviews
All contributions have to be reviewed by someone. It could be that your contribution can be merged immediately by a maintainer. However, usually, a new Merge Request needs some improvements before it can be merged. Other contributors (or our automatic testing jobs) might have feedback. If this is the case the reviewing maintainer will help you improve your documentation and code.

#### 4.2. Definition of Done

With MR's we make User Stories become reality. A User Story is DONE when:
- Code has been written.
- Unit tests have been added that test the behaviour of newly added feature.
- Documentation has been added or updated where necessary.
- The work on the feature has been announced on the #open-accounting Slack channel.
- All changes have been reviewed and approved by another developer.
- Any spin-off user stories have been added to the backlog.
- Product owner has accepted the user story.
- Code has been merged to master.

### 5. Celebrate

Your ideas, documentation and code have become an integral part of this project. You are the Open Source hero we need.

---

For more information on how to use and contribute to this project, please read the [`README`](README.md).
