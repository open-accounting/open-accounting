# Back end

This is the back end for the [open-accounting](https://gitlab.com/open-accounting/) application.

## Project setup

```bash
npm install
```

### Start docker containers

```bash
docker-compose up -d
```

### Start Hasura Console

```bash
npm run hasura:console
```

### Apply migratrions to Hasura

```bash
npm run hasura:push
```

### Create migration file from Hasura

```bash
npm run hasura:pull
```

### Demo data script:

```bash
back-end/graphql/demodata.graphql
```
