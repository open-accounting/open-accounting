# open-accounting

## Architecture Decision Records
Every significant decision will be recorded in an Architecture Decision Record (ADR). These can be found in the `doc/architecture/decisions` directory.
More about ADR: http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions
To make life easy, we use: https://github.com/npryce/adr-tools
